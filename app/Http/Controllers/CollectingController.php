<?php

namespace App\Http\Controllers;

use App\Services\Collectors\FoursquareCollector;
use App\Services\Collectors\YelpCollector;
use App\Services\DataCollectorService;
use App\Services\GlobalHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CollectingController extends Controller
{


    public function collect(Request $request, DataCollectorService $collectorService){
        \session()->flash('process', ['Starting collecting process...']);
        $params = $request->validate([
            'distance' => ['required', 'integer'],
            'lat' => ['required'],
            'long' => ['required'],
        ]);
        $returned = $collectorService->collect($params);
        GlobalHelper::addProcessMessage("Collecting of data done, start generate excel file");
        $spreadsheet = new Spreadsheet();
        // Make Foursquare sheet
        $this->sheetMaker('Foursquare', $spreadsheet, $returned['foursquare']);
        $spreadsheet->setActiveSheetIndexByName('Foursquare');

        // Make Yelp sheet
        $this->sheetMaker('Yelp', $spreadsheet, $returned['yelp']);

        // Make Here sheet
        $this->sheetMaker('Here', $spreadsheet, $returned['here']);

//        // Make Google sheet
        $this->sheetMaker('Google', $spreadsheet, $returned['google']);

        // Make MapBox sheet
        $this->sheetMaker('MapBox', $spreadsheet, $returned['mapbox']);

        // Make Radar sheet
        $this->sheetMaker('Radar', $spreadsheet, $returned['radar']);
        $this->comparisonSheetMaker($spreadsheet, $returned);
        GlobalHelper::addProcessMessage("Done, enjoy");
        $xlsx = new Xlsx($spreadsheet);
        $fileName = "collections/".Carbon::now()->timestamp.'.xlsx';
        $xlsx->save("$fileName");
        return response(['file_url' => "/{$fileName}"]);
    }

    protected function writeHeadersRowIntoSheet($sheet, $fields, $data, $rowIndex){
        try {
            foreach ($fields as $index => $field) {
                $sheet->setCellValue($field . $rowIndex, $data[$index]);
            }
        }catch (\Throwable $t){
            dd($fields, $data, $t);
        }
    }

    protected function writeHeadersRowIntoSheetForComparison($sheet, $field, $data, $rowIndex){
            $sheet->setCellValue($field.$rowIndex, $data);
    }

    protected function writeDataRowIntoSheet($sheet, $fields, $data, $rowIndex){
        foreach ($fields as $index => $field) {
            $sheet->setCellValue($field.$rowIndex, $data[$index]);
        }
    }

    protected function sheetMaker(string $sheetName, Spreadsheet $spreadsheet, array $data){
        $columnsHeader = [ 'Name', 'Full Address', 'Categories', 'Distance'];
        $columns = ['A', 'B', 'C', 'D'];

        $currentRow = 1;

        $newSheet = new Worksheet(null, "{$sheetName}");
        $this->writeHeadersRowIntoSheet($newSheet, $columns, $columnsHeader, $currentRow);
        if(!empty($data['message'])) {
            $this->writeHeadersRowIntoSheet($newSheet, $columns, [$data['message'], '', '', ''], $currentRow);
            $currentRow++;
        }
        foreach ($data['response'] as $entity) {
            $currentRow++;
            $this->writeDataRowIntoSheet($newSheet, $columns, $entity, $currentRow);
        }
        $spreadsheet->addSheet($newSheet);
    }

    protected function comparisonSheetMaker(Spreadsheet $spreadsheet, array $data){
        try {
            $columnsHeader = [];
            foreach ($data as $providerName => $response) {
                $columnsHeader[] = $providerName;
            }
            $columns = ['A', 'B', 'C', 'D', 'E', 'F'];

            $currentRow = 1;

            $newSheet = new Worksheet(null, "Comparison");
            $this->writeHeadersRowIntoSheet($newSheet, $columns, $columnsHeader, $currentRow);

            $currentColumn = 0;
            foreach ($data as $provider) {
                $currentRow = 2;
                foreach ($provider['response'] as $response) {
                    $this->writeHeadersRowIntoSheetForComparison($newSheet, $columns[$currentColumn], $response[0], $currentRow);
                    $currentRow++;
                }
                $currentColumn++;
            }
            $spreadsheet->addSheet($newSheet);
        }catch(\Throwable $t){
            dd($t);
        }
    }

    public function getProcess(){
        $isProcessExist = (bool)\session('process');
        $messages = \session('process');
        \session('process', []);
        return response([
            'active' => $isProcessExist,
            'messages' => $messages
        ]);
    }
}
