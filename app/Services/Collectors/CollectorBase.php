<?php


namespace App\Services\Collectors;


use GuzzleHttp\Client;

abstract class CollectorBase
{


    protected $httpClient = null;
    protected $api = null;

    protected $lang = 0;
    protected $long = 0;
    protected $distance = 0;

    protected $url = null;

    public function __construct($lang, $long, $distance){
        $this->lang = $lang;
        $this->long = $long;
        $this->distance = $distance;
        $this->httpClient = new Client();
    }
    abstract public function collect();
    abstract protected function collectPage($offset=null);
}
