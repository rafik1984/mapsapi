<?php


namespace App\Services\Collectors;


use App\Services\GlobalHelper;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Integer;

class FoursquareCollector extends CollectorBase
{
    protected $clientId = null;
    protected $clientSecret = null;
    protected $v = null;


    public function __construct($lang, $long, $distance){
        parent::__construct($lang, $long, $distance);
        $this->api = env('FOURSQUARE_API');
        $this->clientId = env('FOURSQUARE_CLIENT_ID');
        $this->clientSecret = env('FOURSQUARE_CLIENT_SECRET');
        $this->v = env('FOURSQUARE_V');
        $this->url = "https://api.foursquare.com/v2/venues/search?client_id={$this->clientId}&client_secret={$this->clientSecret}&v={$this->v}&ll={$lang},{$long}&radius={$this->distance}&limit=10000";
        GlobalHelper::addProcessMessage("Start collecting from Foursquare...");
    }

    public function collect(){
        $retCollected = [];
        $collected = $this->collectPage();
        foreach ($collected as $item) {
            if(!empty($item['location']['distance']) && $item['location']['distance'] <= $this->distance) {
                $retCollected[] = [
                    $item['name'],
                    !empty($item['location']['formattedAddress']) ? implode(', ', $item['location']['formattedAddress']) : "No address",
                    !empty($item['categories']) ? $item['categories'][0]['name'] : "No category",
                    $item['location']['distance']
                ];
            }
        }
        array_multisort(array_map(function($element) {
            return $element[0];
        }, $retCollected), SORT_ASC, $retCollected);
        $count = count($retCollected);
        GlobalHelper::addProcessMessage("Count of API calls: 1");
        GlobalHelper::addProcessMessage("Returned count of places is {$count}");
        GlobalHelper::addProcessMessage("\n_________________");
        return ['message' => '', 'response' => $retCollected];
    }

    protected function collectPage($offset = null) :array {
        GlobalHelper::addProcessMessage("Url: {$this->url}");
        $response = json_decode(($this->httpClient->get($this->url)->getBody()), true);
        if(!empty($response['meta']['code']) && $response['meta']['code'] === 200){
            return $response['response']['venues'];
        }
        return [];
    }
}
