<?php


namespace App\Services\Collectors;


use App\Services\GlobalHelper;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Integer;

class GooglePlaceCollector extends CollectorBase
{
    protected $token = null;

    public function __construct($lang, $long, $distance){
        parent::__construct($lang, $long, $distance);
        $this->api = env('GOOGLE_API');
        $this->token = env('GOOGLE_KEY');
        $this->url = "{$this->api}key={$this->token}&location={$this->lang},{$this->long}&limit=100&radius={$this->distance}";
        GlobalHelper::addProcessMessage("Start collecting places from Google");
    }

    public function collect(){
        set_time_limit(600);
        $retCollected = [];
        $message = "";
        $nextPageToken = "";
        $callsCount = 0;
        try {
            do {
                $callsCount++;
                $collected = $this->collectPage($nextPageToken);
                if(!empty($collected['data'])){
                    $this->formatArrays($retCollected, $collected['data']);
                }
                if(!empty($collected['next_page'])) {
                    $nextPageToken = "pagetoken={$collected['next_page']}";
                }else{
                    break;
                }
                sleep(2);
            }while(!empty($collected));
        } catch (\Throwable $throwable) {
            $message = "We unable to load all data from Google, some internal issue on their side, try later";
        }
        array_multisort(array_map(function($element) {
            return $element[0];
        }, $retCollected), SORT_ASC, $retCollected);
        $count = count($retCollected);
        GlobalHelper::addProcessMessage("Count of API calls: {$callsCount}");
        GlobalHelper::addProcessMessage("Total count: {$count}");
        GlobalHelper::addProcessMessage("\n_________________");

        return ['message' => $message, 'response' => $retCollected];
    }

    protected function collectPage($offset = null): array
    {
        $url = $this->url . ($offset ? "&{$offset}" : '');
        $count = 0;
        GlobalHelper::addProcessMessage("Url: {$url}");
        $response = json_decode(($this->httpClient->get($this->url . ($offset ? "&{$offset}" : ''))->getBody()), true);
        if ($response['status'] === "OK" && !empty($response['results'])) {
            foreach ($response['results'] as &$result) {
                $lang = $result['geometry']['location']['lng'];
                $lat = $result['geometry']['location']['lat'];
                $result['distance'] = GlobalHelper::distance($this->lang, $this->long, $lat, $lang);
            }
            $count = count($response['results']);
            GlobalHelper::addProcessMessage("Count of places: {$count}");
            return ['next_page' => (!empty($response['next_page_token']) ? $response['next_page_token'] : ''), 'data' => $response['results']];
        }
        GlobalHelper::addProcessMessage("Count of places: {$count}");
        return [];
    }

    protected function formatArrays(&$retArray, $response){
        foreach ($response as $item) {
            $retArray[] = [
                $item['name'],
                !empty($item['vicinity']) ? $item['vicinity'] : 'No specified address',
                !empty($item['types']) ? implode(',', $item['types']) : 'No specified categories',
                $item['distance']
            ];
        }
    }
}
