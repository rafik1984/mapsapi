<?php


namespace App\Services\Collectors;


use App\Services\GlobalHelper;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Integer;

class HereCollector extends CollectorBase
{
    protected $token = null;

    public function __construct($lang, $long, $distance){
        parent::__construct($lang, $long, $distance);
        $this->api = env('HERE_API');
        $this->token = env('HERE_API_KEY');
        $this->url = "{$this->api}apiKey={$this->token}&at={$this->lang},{$this->long}&limit=100&in=circle:{$this->lang},{$this->long};r={$distance}";
        GlobalHelper::addProcessMessage("Start collecting from Here");
    }

    public function collect(){
        set_time_limit(600);
        $retCollected = [];
        $message = "";
        try {
            $collected = $this->collectPage();
            $this->formatArrays($retCollected, $collected);
        } catch (\Throwable $throwable) {
            $message = "We unable to load all data from Here, some internal issue on their side, try later";
        }
        array_multisort(array_map(function($element) {
            return $element[0];
        }, $retCollected), SORT_ASC, $retCollected);
        $count = count($retCollected);
        GlobalHelper::addProcessMessage("Count of API calls: 1");
        GlobalHelper::addProcessMessage("Total count of places: {$count}");
        GlobalHelper::addProcessMessage("\n_________________");

        return ['message' => $message, 'response' => $retCollected];
    }

    protected function collectPage($offset=null) :array {
        GlobalHelper::addProcessMessage("Url: {$this->url}");
        $response = json_decode(($this->httpClient->get($this->url)->getBody()), true);
        if(!empty($response['items'])){
            return $response['items'];
        }
        return [];
    }

    protected function formatArrays(&$retArray, $response){
        foreach ($response as $item) {
            $retArray[] = [
                $item['title'],
                !empty($item['address']['label']) ? $item['address']['label'] : 'No specified address',
                !empty($item['categories']) ? implode(',', array_column($item['categories'], 'name')) : 'No specified categories',
                !empty($item['distance']) ? $item['distance'] : 'No specified distance'
            ];
        }
    }
}
