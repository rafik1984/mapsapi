<?php


namespace App\Services\Collectors;


use App\Services\GlobalHelper;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Integer;

class MapBoxCollector extends CollectorBase
{
    protected $token = null;
    protected $bbox = [];
    protected $interests = [["restaurant", "shopping center", "mall", "shopping mall", "spa", "sporting good", "sports store", "supermarket", "groceries", "market", "services"]];


    public function __construct($lang, $long, $distance){
        parent::__construct($lang, $long, $distance);
        $this->api = env('MAPBOX_API');
        $this->token = env('MAPBOX_KEY');
        $this->bbox = $this->boundBoxCalculator($lang, $long, $distance);
        GlobalHelper::addProcessMessage("Start collect places from Mapbox");
    }

    public function collect(){
        set_time_limit(600);
        $retCollected = [];
        $message = "";
        try {
            $collected = $this->collectPage();
            $this->formatArrays($retCollected, $collected);
        } catch (\Throwable $throwable) {
            $message = "We unable to load all data from MapBox, some internal issue on their side, try later";
        }
        array_multisort(array_map(function($element) {
            return $element[0];
        }, $retCollected), SORT_ASC, $retCollected);
        $count = count($retCollected);
        $callsCount = count($this->interests);
        GlobalHelper::addProcessMessage("Count of API calls: {$callsCount}");
        GlobalHelper::addProcessMessage("Total count: {$count}");
        GlobalHelper::addProcessMessage("\n_________________");

        return ['message' => $message, 'response' => $retCollected];
    }

    public function collectPage($offset=null) :array {
        $boundBox = implode(",", $this->bbox);
        $response = [];
        $index = 1;
        foreach ($this->interests as $interest) {
            GlobalHelper::addProcessMessage("List of categories N: {$index}");
            $category = implode(",", $interest);
            $url = "{$this->api}{$category}.json?bbox={$boundBox}&limit=10000&access_token={$this->token}";
            GlobalHelper::addProcessMessage("Url: {$url}");
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $output = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);
            $output = json_decode($output, true);
            if(!empty($output['features'])){
                $count = count($output['features']);
                GlobalHelper::addProcessMessage("Count of collected places: {$count}");
                $response = array_merge($response, $output['features']);
            }
            $index++;
        }
        return $response;
    }

    protected function formatArrays(&$retArray, $response){
        foreach ($response as $item) {
            $retArray[] = [
                $item['text'],
                !empty($item['properties']['address']) ? $item['properties']['address'] : $item['place_name'],
                !empty($item['properties']['category']) ? $item['properties']['category'] : 'No specified categories',
                "No data"
            ];
        }
    }

    protected function boundBoxCalculator($lang, $long, $distance)
    {
            $R = 6378.14;
            $distance = $distance/1000+1;
            $latitude1 = $lang * (M_PI/180);
            $longitude1 = $long * (M_PI/180);
            $brng = $distance * (M_PI/180);

            $latitude2 = asin(sin($latitude1)*cos($distance/$R) + cos($latitude1)*sin($distance/$R)*cos($brng));
            $longitude2 = $longitude1 + atan2(sin($brng)*sin($distance/$R)*cos($latitude1),cos($distance/$R)-sin($latitude1)*sin($latitude2));

            $latitude2 = $latitude2 * (180/M_PI);
            $longitude2 = $longitude2 * (180/M_PI);

            $lat2 = round ($latitude2,6);
            $long2 = round ($longitude2,6);

            return [
                $long, $lang, $long2, $lat2
            ];
    }
}
