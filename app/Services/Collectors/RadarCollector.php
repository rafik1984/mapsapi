<?php


namespace App\Services\Collectors;


use App\Services\GlobalHelper;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Integer;

class RadarCollector extends CollectorBase
{
    protected $token = null;
    protected $api = null;

    protected $categories = ["local-services,shopping-retail,education,finance,food-beverage",
        "arts-entertainment,hotel-lodging,medical-health,real-estate","spa-beauty-personal-care,sports-recreation,travel-transportation"];

    public function __construct($lang, $long, $distance){
        parent::__construct($lang, $long, $distance);
        $this->api = env('RADAR_API');
        $this->token = env('RADAR_KEY');
        GlobalHelper::addProcessMessage("Start collect places from Radar");
    }

    public function collect(){
        set_time_limit(600);
        $retCollected = [];
        $callsCount = count($this->categories);
        $message = "";
        try {
            $collected = $this->collectPage();
            $this->formatArrays($retCollected, $collected);
        } catch (\Throwable $throwable) {
            $message = "We unable to load all data from MapBox, some internal issue on their side, try later";
        }
        array_multisort(array_map(function($element) {
            return $element[0];
        }, $retCollected), SORT_ASC, $retCollected);
        $count = count($retCollected);
        GlobalHelper::addProcessMessage("Count of API calls: {$callsCount}");
        GlobalHelper::addProcessMessage("Total count of places from Radar: {$count}");
        GlobalHelper::addProcessMessage("\n_________________");

        return ['message' => $message, 'response' => $retCollected];
    }

    public function collectPage($offset=null) :array {
        $response = [];
        foreach ($this->categories as $category) {
            $url = "{$this->api}?near={$this->lang},{$this->long}&radius={$this->distance}&categories={$category}";
            GlobalHelper::addProcessMessage("Category: {$category}");
            GlobalHelper::addProcessMessage("Url: {$url}");
            $ch = curl_init();
            $header = [
                "Authorization: {$this->token}"
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $output = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);
            $output = json_decode($output, true);
            if(!empty($output['meta']) && $output['meta']['code'] === 200){
                $count = count($output['places']);
                GlobalHelper::addProcessMessage("Count of this request places: {$count}");
                $response = array_merge($response, $output['places']);
            }
        }
        return $response;
    }

    protected function formatArrays(&$retArray, $response){
        foreach ($response as $item) {
            $lat2 = !empty($item['location']['coordinates']) ? $item['location']['coordinates'][1] : $this->lang;
            $long2 = !empty($item['location']['coordinates']) ? $item['location']['coordinates'][0] : $this->long;
            $retArray[] = [
                $item['name'],
                "-",
                implode(',', $item['categories']),
                GlobalHelper::distance( $this->lang, $this->long, $lat2, $long2)
            ];
        }
    }
}
