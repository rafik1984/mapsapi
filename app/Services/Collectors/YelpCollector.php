<?php


namespace App\Services\Collectors;


use App\Services\GlobalHelper;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Integer;

class YelpCollector extends CollectorBase
{
    protected $token = null;

    protected $collectedIds = [];

    public function __construct($lang, $long, $distance){
        parent::__construct($lang, $long, $distance);
        $this->api = env('YELP_API');
        $this->token = env('YELP_TOKEN');
        $this->url = "{$this->api}latitude={$this->lang}&longitude={$this->long}&limit=50&radius={$distance}";
        GlobalHelper::addProcessMessage("Start collecting data from Yelp...");
    }

    public function collect(){
        set_time_limit(600);
        $retCollected = [];
        $collected = [];
        $currentOffset = 0;
        $message = "";
        do {
            try {
                $collected = $this->collectPage($currentOffset);
                $this->formatArrays($retCollected, $collected);
            } catch (\Throwable $throwable) {
                $message = "We unable to load all data from Yelp, some internal issue on their side, try later";
            }

            $currentOffset++;
        } while (!empty($collected));
        array_multisort(array_map(function($element) {
            return $element[0];
        }, $retCollected), SORT_ASC, $retCollected);
        $count = count($retCollected);
        GlobalHelper::addProcessMessage("Count of API calls: {$currentOffset}");
        GlobalHelper::addProcessMessage("Total count of places: {$count}");
        GlobalHelper::addProcessMessage("\n_________________");

        return ['message' => $message, 'response' => $retCollected];
    }

    protected function collectPage($offset=null) :array {
        $headers = [
            'headers' => [
                'Authorization' => "Bearer {$this->token}"
            ]
        ];
        GlobalHelper::addProcessMessage("Offset: {$offset}");
        $url = $this->url."&offset={$offset}";
        GlobalHelper::addProcessMessage("Url: {$url}");
        $response = json_decode(($this->httpClient->get($this->url."&offset={$offset}", $headers)->getBody()), true);
        $count = 0;
        if(!empty($response['businesses'])){
            $count = count($response['businesses']);
            GlobalHelper::addProcessMessage("Count of returned places: {$count}");
            return $response['businesses'];
        }
        GlobalHelper::addProcessMessage("Count of returned places: {$count}");
        return [];
    }

    protected function formatArrays(&$retArray, $response){
        foreach ($response as $item) {
            if(!in_array($item['id'], $this->collectedIds)) {
                $retArray[] = [
                    $item['name'],
                    implode(' ', $item['location']['display_address']),
                    implode(',', array_column($item['categories'], 'title')),
                    $item['distance']
                ];
                $this->collectedIds[] = $item['id'];
            }
        }
    }
}
