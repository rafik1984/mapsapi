<?php


namespace App\Services;


use App\Services\Collectors\FoursquareCollector;
use App\Services\Collectors\GooglePlaceCollector;
use App\Services\Collectors\HereCollector;
use App\Services\Collectors\MapBoxCollector;
use App\Services\Collectors\RadarCollector;
use App\Services\Collectors\YelpCollector;

class DataCollectorService
{
    protected $cities = [
        // Boston, MA
        1 => ['lang' => 42.361145, 'long'=> -71.057083],
        // Lexington, MA
        2 => ['lang' => 42.443037, 'long' => -71.228964],
        // New York, NY
        3 => ['lang' => 40.730610, 'long' => -73.935242],
    ];

    public function collect(array $params):array{
        $currentCity = [
            'lang' => $params['lat'],
            'long' => $params['long']
        ];

        // Collect data from Foursquare
        $f_response = (new FoursquareCollector($currentCity['lang'], $currentCity['long'], $params['distance']))->collect();
        // Collect data from Yelp
        $y_response = (new YelpCollector($currentCity['lang'], $currentCity['long'], $params['distance']))->collect();
        // Collect data from Here
        $h_response = (new HereCollector($currentCity['lang'], $currentCity['long'], $params['distance']))->collect();
        // Collect data from Google
        $g_response = (new GooglePlaceCollector($currentCity['lang'], $currentCity['long'], $params['distance']))->collect();
        // Collect data from MapBox
        $m_response = (new MapBoxCollector($currentCity['lang'], $currentCity['long'], $params['distance']))->collect();
        // Collect data from Radar.io
        $r_response = (new RadarCollector($currentCity['lang'], $currentCity['long'], $params['distance']))->collect();
        return [
            'here' => $h_response,
            'yelp' => $y_response,
            'foursquare' => $f_response,
            'google' => $g_response,
            'mapbox' => $m_response,
            'radar' => $r_response
        ];
    }
}
