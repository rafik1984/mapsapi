<?php


namespace App\Services;


class GlobalHelper
{

    static public function addProcessMessage(string $message){
        \session()->flash('process', array_merge(\session()->get('process'), [$message]));
    }

    static public function distance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
            $miles = acos($miles);
            $miles = rad2deg($miles);
            $miles = $miles * 60 * 1.1515;
            $kilometers = $miles * 1.609344;
            return round(($kilometers * 1000), 2);

        }
    }
}
