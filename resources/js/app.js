require('./bootstrap');
var messages = "";

$("#start-collect").on("click", function (e){
        e.preventDefault();
        let form = $(document).find("#collecting-form");
        let lang = form.find('#latitude').val();
        let long = form.find('#longitude').val();
        let distance = form.find('#distance').val();
        let url = form.attr('action');
        url+="?lat="+lang+"&long="+long+"&distance="+distance;
        if(lang === "" || long === "" || distance === 0){
            alert("Please enter all fields - latitude, longitude, distance");
            return;
        }
        $("#log").text("Start collecting, please wait...");
        startChecking();
        startCollecting(url);
})


function startChecking() {
    setInterval(function () {
        $.get("/process").then(data => {
            let element = $("#log");
            if (data['active']) {
                $.each(data['messages'], function (index, value){
                    messages+=value+"\n";
                })
                $(element).text(messages)
            } else {
                console.log("No active process\n");
                console.log(data);
            }
        });
    }, 2000);
}

function startCollecting(url){
    $.get(url, function (response){
        console.log(response);
        let downloadForm = $(document).find("#download-form");
        downloadForm.attr('action', response.file_url)
        downloadForm.find('button').removeAttr('disabled');
    })
}

