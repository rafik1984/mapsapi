<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Something just to change default title</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="container">
                <div class="row">
                <form action="{{route("collect")}}" id="collecting-form">
                    <div class="row">
                        <div class="col-6 my-1">
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="Latitude">Latitude</label>
                                    <input type="text" class="form-control" id="latitude" aria-describedby="emailHelp" placeholder="Please enter Latitude" name="lat" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="Longitude">Longitude</label>
                                    <input type="text" class="form-control" id="longitude" aria-describedby="emailHelp" placeholder="Please enter Longitude" name="long" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 my-1">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Distances</label>
                            <select class="custom-select mr-sm-2" id="distance" name="distance" required>
                                <option selected value="0">Choose...</option>
                                <option value="100">100 meters</option>
                                <option value="250">250 meters</option>
                                <option value="1000">1 kilometer</option>
                            </select>
                        </div>
                    </div>
                </form>
                </div>
                <div class="row d-flex">
                    <button type="submit" class="btn btn-primary d-flex mr-2" id="start-collect">Collect places</button>

                    <form action="" id="download-form" class="d-flex">
                        <button type="submit" class="btn btn-primary" id="download" disabled="disabled">Download</button>
                    </form>
                </div>

            </div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <textarea name="log" id="log" cols="120" rows="10" disabled>

                </textarea>
            </div>
        </div>

        <script src="{{ asset('js/app.js') }}" defer></script>
    </body>
</html>
